extends ScrollContainer


@export var back_path: String
var mode = "Advanced"


func _on_back_button_pressed() -> void:
	get_tree().change_scene_to_file(back_path)


func _on_export_button_pressed() -> void:
	var export_string := ""
	var days: Array[Node] = %Week.get_days()
	var i := 0
	while i < len(days):
		export_string += days[i].get_day_hex_string()
		i += 1
		if i < len(days):
			export_string += " "
	if mode == "Simple":
		export_string = simple_mode(export_string)
	%ExportText.text = export_string
	%ExportPopup.popup_centered()

func simple_mode(original_string: String) -> String:
	var elements = original_string.split(" ")
	var weekday = elements[0]
	var weekend = elements[1]
	elements.clear()
	elements.insert(0, weekend)
	for i in range(5):
		elements.insert(i+1, weekday)
	elements.insert(6, weekend)
	var new_string = ""
	for i in range(elements.size()):
		new_string += elements[i]
		if i < elements.size() - 1:
			new_string += " "
	return new_string

func _on_simple_button_toggled(button_pressed):
	if button_pressed == true:
		mode = "Simple"
		var day = $HBoxContainer/Week/HBoxContainer/Day
		for i in range(5):
			get_node(str(day.get_path()) + str(i+3)).visible = false
		
		var label = "/VBoxContainer/DayLabel"
		get_node(str(day.get_path()) + label).text = "Weekdays"
		get_node(str(day.get_path()) + str(2) + label).text = "Weekends"
	elif button_pressed == false:
		mode = "Advanced"
		var day = $HBoxContainer/Week/HBoxContainer/Day
		for i in range(5):
			get_node(str(day.get_path()) + str(i+3)).visible = true
		
		var label = "/VBoxContainer/DayLabel"
		get_node(str(day.get_path()) + label).text = "Sunday"
		get_node(str(day.get_path()) + str(2) + label).text = "Monday"
